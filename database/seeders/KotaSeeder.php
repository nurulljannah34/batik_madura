<?php

namespace Database\Seeders;

use App\Models\Kota;
use Illuminate\Database\Seeder;

class KotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kota::create([
            "name" => "Pamekasan"
        ]);
        Kota::create([
            "name" => "Sumenep"
        ]);
        Kota::create([
            "name" => "Sampang"
        ]);
        Kota::create([
            "name" => "Bangkalan"
        ]);
    }
}
