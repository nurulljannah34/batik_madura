<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGambarBatiksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gambar_batiks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("batik_id");
            $table->string("gambar");
            $table->text("keterangan")->nullable();
            $table->foreign("batik_id")->on("batiks")->references("id")->onUpdate("cascade")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gambar_batiks');
    }
}
