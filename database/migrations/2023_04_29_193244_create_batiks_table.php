<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatiksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batiks', function (Blueprint $table) {
            $table->id();
            $table->string("kode_batik")->unique();
            $table->string("nama_batik");
            $table->string("motif_batik");
            $table->unsignedBigInteger("kota_id"); // asal batik
            $table->text("deskripsi")->nullable();

            $table->foreign("kota_id")->on("kotas")->references("id")->onUpdate("cascade")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batiks');
    }
}
