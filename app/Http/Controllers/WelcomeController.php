<?php

namespace App\Http\Controllers;

use App\Models\Batik;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index( Request $request){
        if ($request->cari){
            $batik = Batik::query()
                ->where("nama_batik", "like", "%$request->cari%")
                ->orWhere("kode_batik", "like", "%$request->cari%")
                ->paginate(9)->withQueryString();
        }elseif ($request->kota){
            $batik =  Batik::where("kota_id", '=', $request->kota)->paginate(9)->withQueryString();
        }

        else{
            $batik = Batik::paginate(9);
        }
        return view("welcome.welcome", compact("batik"));
    }
    public function show(Batik $batik){
        return view("welcome.show", compact("batik"));
    }
}
