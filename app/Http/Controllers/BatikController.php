<?php

namespace App\Http\Controllers;

use App\DataTables\BatikDataTable;
use App\Models\Batik;
use App\Models\GambarBatik;
use App\Models\Kota;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BatikController extends Controller
{
    public function index(BatikDataTable $dataTable)
    {
        return $dataTable->render("batik.index");
    }

    public function show(Batik $batik)
    {
        return view("batik.show", compact("batik"));
    }
    public function uploadFoto(Batik $batik, Request $request)
    {
        if (!$request->foto) {
            return back();
        }
        $fotos = $request->file('foto');
        if (!$fotos) {
            return back();
        }
        $nameFotos = array();
        foreach ($fotos as $item) {
            $nameFoto = Str::random('5') . ' ' . $item->getClientOriginalName();
            $item->move('batikGambar', $nameFoto);
            $nameFotos[] = $nameFoto;
        }
        foreach ($nameFotos as $item) {
            GambarBatik::create([
                'batik_id' => $batik->id,
                'gambar' => $item,
            ]);
        }
        return back();
    }

    public function add()
    {
        $kota = Kota::all();
        return view("batik.add", compact("kota"));
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'kota_id' => 'required',
            'nama_batik' => 'required',
            'motif_batik' => 'required',
            'deskribsi' => 'required',
        ]);
        $photo = $request->file('gambar_batik');

        $kode = "";
        $kota_id = $request->kota_id;
        if ($kota_id == "1") {
            $kode = "PMKS-" . Str::random("6");
        } else if ($kota_id == "2") {
            $kode = "SMNP-" . Str::random("6");
        } else if ($kota_id == "3") {
            $kode = "SMPG-" . Str::random("6");
        } else {
            $kode = "BKLN-" . Str::random("6");
        }
        $batik = Batik::create([
            "kota_id" => $request->kota_id,
            "nama_batik" => $request->nama_batik,
            "motif_batik" => $request->motif_batik,
            "deskripsi" => $request->deskribsi,
            "kode_batik" => $kode,
        ]);

        if (!$batik) {
            return back();
        }
        if ($photo) {
            $nameFoto = Str::random('5') . ' ' . $photo->getClientOriginalName();
            $photo->move('batikGambar', $nameFoto);
            GambarBatik::create([
                "batik_id" => $batik->id,
                "gambar" => $nameFoto,
            ]);
        }
        return redirect()->route("batik.index");
    }
    public function edit(Batik $batik)
    {
        $kota = Kota::all()->where("id", "!=", $batik->kota_id);
        return view("batik.edit", compact("batik", "kota"));
    }
    public function update(Batik $batik, Request $request)
    {
        $photo = $request->file('gambar_batik');

        if ($request->kota_id != $batik->kota_id) {
            $kode = "";
            $kota_id = $request->kota_id;
            if ($kota_id == "1") {
                $kode = "PMKS-" . Str::random("6");
            } else if ($kota_id == "2") {
                $kode = "SMNP-" . Str::random("6");
            } else if ($kota_id == "3") {
                $kode = "SMPG-" . Str::random("6");
            } else {
                $kode = "BKLN-" . Str::random("6");
            }
            $batikUpdate = $batik->update([
                "kota_id" => $request->kota_id,
                "nama_batik" => $request->nama_batik,
                "motif_batik" => $request->motif_batik,
                "deskripsi" => $request->deskribsi,
                "kode_batik" => $kode,
            ]);
        } else {
            $batikUpdate = $batik->update([
                "kota_id" => $request->kota_id,
                "nama_batik" => $request->nama_batik,
                "motif_batik" => $request->motif_batik,
                "deskripsi" => $request->deskribsi,
            ]);
        }

        if (!$batikUpdate) {
            return back();
        }
        if ($photo) {
            $nameFoto = Str::random('5') . ' ' . $photo->getClientOriginalName();
            $photo->move('batikGambar', $nameFoto);
            $oldImage = GambarBatik::where("batik_id", "=", $batik->id)->first();
            $oldImage->delete();
            GambarBatik::create([
                "batik_id" => $batik->id,
                "gambar" => $nameFoto,
            ]);
        }
        return redirect()->route("batik.index");

    }
    public function destroy(Batik $batik)
    {
        if (!$batik) {
            return response()->json([
                'status' => 'gagal',
                'massage' => 'batik gagal di hapus',
            ]);
        }
        $batik->delete();
        return response()->json([
            'status' => 'success',
            'massage' => 'batik berhasil di hapus',
        ]);

    }
}
