<?php

namespace App\Http\Controllers;

use App\Models\Batik;
use App\Models\Kota;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $allbatik = Batik::all()->count();
        $allkota = Kota::all()->count();
        $batikpamekasan = Batik::all()->where("kota_id", '=', '1')->count();
        $batiksumenep = Batik::all()->where("kota_id", '=', '2')->count();
        $batiksampang = Batik::all()->where("kota_id", '=', '3')->count();
        $batikbangkalan = Batik::all()->where("kota_id", '=', '4')->count();
        return view("dashboard", compact([
            "allbatik",
            'allkota',
            'batikpamekasan',
            'batiksumenep',
            'batiksampang',
            'batikbangkalan'
        ]));
    }
}
