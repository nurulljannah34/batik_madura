<?php

namespace App\DataTables;

use App\Models\Batik;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class BatikDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn("kota_id", function ($row) {
                return $row->kota->name;
            })
            ->addColumn('action', function ($query) {
                $button = '';
                $button .= '<a href="' . route("batik.show", $query->kode_batik) . '" type="button" class="btn btn-sm btn-outline-primary">Show</a>';
                $button .= '<a href="' . route("batik.edit", $query->kode_batik) . '" type="button" class="m-2 btn btn-sm btn-outline-warning">Edit</a>';
                $button .= '<button type="button" data-type="delete" data-id=' . $query->kode_batik . ' class="m-2 btn btn-sm btn-outline-danger action">Delete</button>';
                return $button;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Batik $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Batik $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('batik-table')
            ->columns($this->getColumns())
//                    ->minifiedAjax()
            ->dom('Bfrtip')
            ->addTableClass("table table-bordered dataTable")
            ->orderBy(1)
            ->autoWidth(false)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('kode_batik'),
            Column::make('nama_batik'),
            Column::make('motif_batik'),
            Column::make('kota_id')->title("Kota"),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Batik_' . date('YmdHis');
    }
}
