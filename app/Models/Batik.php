<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Batik extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'kode_batik';
    }

    public function gambar()
    {
        return $this->hasMany(GambarBatik::class, 'batik_id', 'id');
    }
    public function kota()
    {
        return $this->hasOne(Kota::class, "id", "kota_id");
    }
}
