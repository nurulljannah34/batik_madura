<?php

use App\Http\Controllers\BatikController as BatikControllerAlias;
use App\Http\Controllers\DashboardController as DashboardControllerAlias;
use App\Http\Controllers\WelcomeController as WelcomeControllerAlias;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(['middleware' => [], 'prefix' => '/'], function ($router) {
    Route::get("/", [WelcomeControllerAlias::class, 'index'])->name("welcome");
    Route::get("show/{batik}", [WelcomeControllerAlias::class, 'show'])->name("welcome.show");
});

Route::get('/dashboard', [DashboardControllerAlias::class, "index"])->middleware(['auth'])->name('dashboard');

Route::group(['middleware' => ['auth'], 'prefix' => 'batik'], function ($router) {
    Route::get("/", [BatikControllerAlias::class, 'index'])->name("batik.index");
    Route::get("show/{batik}", [BatikControllerAlias::class, 'show'])->name("batik.show");
    Route::post("add/foto/{batik}", [BatikControllerAlias::class, 'uploadFoto'])->name("batik.addFoto");
    Route::get("add", [BatikControllerAlias::class, 'add'])->name("batik.add");
    Route::post("add", [BatikControllerAlias::class, 'store'])->name("batik.store");
    Route::get("{batik}/edit", [BatikControllerAlias::class, "edit"])->name("batik.edit");
    Route::post("{batik}/update", [BatikControllerAlias::class, "update"])->name("batik.update");
    Route::delete("delete/{batik}", [BatikControllerAlias::class, "destroy"])->name("batik.delete");
});

require __DIR__ . '/auth.php';
