<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('dashboard')}}">
        <div class="sidebar-brand-text mx-3">Admin Panel<sup>V0</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{request()->segment(1) == "dashboard" ? 'active' : ''}}">
        <a class="nav-link" href="{{route("dashboard")}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>



    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Data
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{request()->segment(1) == "batik" ? 'active' : ''}}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#kegiatan"
           aria-expanded="true" aria-controls="kegiatan">
            <i class="fas fa-fw fa-archway"></i>
            <span>Batik</span>
        </a>
        <div id="kegiatan" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Menu</h6>
                <a class="collapse-item" href="{{route("batik.add")}}">Tambah Batik</a>
                <a class="collapse-item" href="{{route("batik.index")}}">List Batik</a>
            </div>
        </div>
    </li>





</ul>
<!-- End of Sidebar -->
