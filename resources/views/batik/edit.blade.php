@extends('panels.master')

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Batik</h6>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('batik.update', $batik->kode_batik) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="kota_id">Pilih Kota Batik</label>
                        <select class="form-control" name="kota_id" id="kota_id">
                            <option value="{{ $batik->kota_id }}">{{ $batik->kota->name }}</option>
                            @foreach ($kota as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nama_batik">Nama Batik</label>
                        <input type="text" class="form-control" name="nama_batik" id="nama_batik"
                            value="{{ $batik->nama_batik }}">
                    </div>
                    <div class="form-group">
                        <label for="motif_batik">Motif Batik</label>
                        <input type="text" class="form-control" name="motif_batik" id="motif_batik"
                            value="{{ $batik->motif_batik }}">
                    </div>
                    <div class="form-group">
                        <label for="deskribsi">Deskribsi Batik</label>
                        <textarea type="text" class="form-control" name="deskribsi" rows="4" id="deskribsi">{{ $batik->deskripsi }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="gambar_batik">Gambar Batik</label>
                        <input type="file" class="form-control-file" name="gambar_batik" id="gambar_batik">
                    </div>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </form>
            </div>
        </div>
    </div>
@endsection
