@extends('panels.master')

@push('css')
    <link href="{{ asset('') }}assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.11/dist/sweetalert2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css"
        integrity="sha512-O03ntXoVqaGUTAeAmvQ2YSzkCvclZEcPQu1eqloPaHfJ5RuNGiS4l+3duaidD801P50J28EHyonCV06CUlTSag=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush

@section('content')
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data Kegiatan</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    {{ $dataTable->table() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('') }}assets/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('') }}assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.11/dist/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"
        integrity="sha512-Zq9o+E00xhhR/7vJ49mxFNJ0KQw1E1TMWkPTxrWcnpfEFDEXgUiwJHIKit93EW/XxE31HSI5GEOW06G6BF1AtA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    {{ $dataTable->scripts() }}

    <script>
        $('#batik-table').on('click', '.action', function() {
            let data = $(this).data();
            let id = data.id;
            let jenis = data.type;

            console.log(jenis);

            if (jenis === 'delete') {
                Swal.fire({
                    title: 'Apakah Kamu Yakin?',
                    text: "Kamu Tidak Akan Bisa Melakukan Restorasi Data!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Hapus!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            method: 'delete',
                            url: `{{ url('batik/delete') }}/${id}`,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(ress) {
                                let table = $('#batik-table').DataTable();
                                table.cleanData;
                                table.ajax.reload();
                                if (ress.status == "success") {
                                    iziToast.success({
                                        title: ress.status,
                                        message: ress.massage,
                                        position: 'topRight'
                                    });
                                } else {
                                    iziToast.error({
                                        title: ress.status,
                                        message: ress.massage,
                                        position: 'topRight'
                                    });
                                }
                            }
                        })
                    }
                })
                return
            }
        })
    </script>
@endpush
