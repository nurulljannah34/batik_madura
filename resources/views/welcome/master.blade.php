<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" href="{{ asset('assets/img/icons.jpg') }}" type="image/x-icon">


    <!-- Custom fonts for this template-->
    <link href="{{ asset('') }}assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <link href="{{ asset('') }}assets/css/sb-admin-2.min.css" rel="stylesheet">
    <style>
        body {
            padding-bottom: 70px;
            /* Height of the footer */
        }
    </style>
</head>

<body>

    <!-- Header Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-info">
        <a class="navbar-brand" href="#">Batik Madura</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('welcome') }}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">Admin</a>
                </li>
                {{--            <li class="nav-item"> --}}
                {{--                <a class="nav-link" href="#">Contact</a> --}}
                {{--            </li> --}}
            </ul>
        </div>
    </nav>

    <!-- Page Content -->
    @yield('content')
    <!-- Footer -->
    <footer class="bg-dark py-3 fixed-bottom">
        <div class="container">
            <p class="text-center text-white">&copy; 2023 Batik Catalog </p>
        </div>
    </footer>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('') }}assets/vendor/jquery/jquery.min.js"></script>
    <script src="{{ asset('') }}assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    @stack('js')

</body>

</html>
