@extends('welcome.master')

@section('content')
    <div class="container">
        <h1 class="text-dark mt-3">Batik Madura</h1>
        <div class="row">
            <div class="col">
                <form id="formsearch" action="{{ route('welcome') }}" method="get">
                    <div class="form-group">
                        <input value="{{ request('cari') }}" type="text" class="form-control" id="search"
                            placeholder="Search..." name="cari">
                        <button type="submit" class="btn btn-info mt-3">Cari</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row d-flex justify-content-center">
            <!-- Catalog items will be dynamically added here -->
            @foreach ($batik as $item)
                <div class="card-deck col-md-4 mt-3">
                    <div class="card">
                        <img src="{{ url('batikGambar' . '/' . $item->gambar->first()->gambar) }}" class="card-img-top"
                            alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{ $item->nama_batik }}</h5>
                            <p class="card-text"><strong>Motif : </strong> {{ $item->motif_batik }}</p>
                            <p class="card-text"><strong>kota : </strong>
                                <a class="card-link"
                                    href="{{ route('welcome', 'kota=' . $item->kota_id) }}">{{ $item->kota->name }}</a>
                            </p>
                            <p class="card-text"><strong>Kode : </strong> {{ $item->kode_batik }}</p>
                        </div>
                        <div class="card-footer">
                            <a href="{{ route('welcome.show', $item->kode_batik) }}" class="card-link">Lihat Detail</a>
                        </div>
                    </div>
                </div>
            @endforeach
            <!-- Repeat for other catalog items -->
        </div>
        <div class="d-flex justify-content-end mt-3">
            {!! $batik->links() !!}
        </div>
    </div>
@endsection
