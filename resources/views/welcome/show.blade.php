@extends("welcome.master")

@section("content")
    <div class="container-fluid">
        <div class="card shadow mb-4 mt-4">
            <a href="#kegiatanshow" class="d-block card-header py-3" data-toggle="collapse"
               role="button" aria-expanded="true" aria-controls="kegiatanshow">
                <h6 class="m-0 font-weight-bold text-primary">Detail Batik</h6>
            </a>
            <div class="collapse show" id="kegiatanshow">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>Kode Batik</th>
                                <td>{{$batik->kode_batik}}</td>
                            </tr>
                            <tr>
                                <th>Nama Batik</th>
                                <td>{{$batik->nama_batik}}</td>
                            </tr>
                            <tr>
                                <th>Motif Batik</th>
                                <td>{{$batik->motif_batik}}</td>
                            </tr>
                            <tr>
                                <th>Kota Batik</th>
                                <td>{{$batik->kota->name}}</td>
                            </tr>
                            <tr>
                                <th>Deskripsi Batik</th>
                                <td class="text-justify">{{$batik->deskripsi}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="card shadow mb-4">
            <a href="#fotoshow" class="d-block card-header py-3" data-toggle="collapse"
               role="button" aria-expanded="true" aria-controls="fotoshow">
                <h6 class="m-0 font-weight-bold text-primary">Foto Batik</h6>
            </a>
            <div class="collapse show" id="fotoshow">
                <div class="card-body">
                    <div class="row">
                        @foreach($batik->gambar as $item)
                            <div class="col-xs-6 col-md-4" >
                                <button data-toggle="modal" data-target="#exampleModalCenter">
                                    <img src="{{asset('').'batikGambar/'. $item->gambar}}" class="img-thumbnail" alt="Image 1">
                                </button>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Show Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="{{asset('').'batikGambar/'. $item->gambar}}" height="500" width="100%">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection


